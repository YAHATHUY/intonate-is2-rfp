# IS2 Vendor RfP

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This repository contains the necessary skeleton code to implement the tasks outlined in the detailed vendor showcase task guide as part of the Intonate IS2 RfP process. You can find two starter notebooks with minimal content for both task 1 and 2. Most of the necessary imports have been included in the starter code. However, you will need to add the remaining imports as required. 

The additional functions such as the sliding window which you will need to modify can be found in `/intonate-is2-rfp/src/utils/`. There is a commented code block in this helper function specifying where you should make the change to be used with the ONNX inference runtime..

 You will not be required to modify the `/intonate-is2-rfp/src/utils/convert_classes.py` function and can be used as is.

## Requirements
* Python==3.10.9
* CUDA>=11.6
* PyTorch>=1.13.1
* MONAI==1.1.0

Tested environment
* Python==3.10.9
* PyTorch==2.0.1
* CUDA==11.7
* cuDNN==8.5.0
* MONAI==1.1.0

 <span style="color:red">**Do not use the latest MONAI version (v1.2.0) as it has unexpected behaviour possibly due to some breaking changes. The code has not been tested for any other MONAI version other than 1.1.0.**</span>

# Installation

Clone with SSH:

```bash
$ git clone git@gitlab.com:YAHATHUY/intonate-is2-rfp.git
```

Clone with HTTPS:

```bash
$ git clone https://gitlab.com/YAHATHUY/intonate-is2-rfp.git
```

## Create Conda Environment
```bash
$ cd intonate-is2-rfp
$ conda env create -f environment.yml
$ conda activate is2_rfp
```
<br>

Download the data from the link provided and save it to the root directory of the project. Please note that the data is only avaiable for a period of **7 days** as mentioned in the task guide. Therefore, please ensure you download the dataset before the 7-day period is over. Once extracted you should have the following tree structure with data for task 1 and 2 stored seperately.

    intonate-is2-rfp
    ├── data
    │   └── train         # data for task 2 (200 images)
    │       └── images
    │       └── labels  
    │   └── valid         # data for task 1 (96 images)
    │       └── images
    │       └── labels  
    ├── models            # folder to save converted ONNX model and trained PyTorch model in task 2
    ├── monai_bundle      # Monai bundle containing the pre-trained model for task 1 & 2
    │   └── configs       # contains model architecture
    │   └── models        # Monai model weights 
    ├── notebooks         # starter notebooks
    ├── preds             # folder to save predictions (NIfTi files)
    ├── src       
    │   └── scripts       # if python scripts are used instead of notebooks save them here        
    │   └── utils         # contains sliding window & channel conversion function

You should save your converted ONNX model and predictions to the specified folders as shown in the tree structure above. If you are performing tasks 1 & 2 using Jupyter notebooks, save them inside the notebooks folder. If it is using Python scripts, save them under the `/intonate-is2-rfp/src/scripts/` folder. Once completed, each vendor should submit the entire code repository containing all relevant code, model files and predictions as a single `.tar.gz` file. 

**Make sure to exclude the data folder when converting to an archive for submission.**

This code repostitory will not be monitored regularly for any issues raised through GitLab since this is a secondary Roche GitLab account used specifically for this RfP process to allow public access to the repository. If you have any questions or issues, please contact the Intonate IS2 team via email.