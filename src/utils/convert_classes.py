import torch
from monai.transforms import apply_transform, MapTransform


class ConvertToMultiChannelClassesd(MapTransform):
    """
    Convert labels to multi channel classes:
    label 0 is the Tumor core
    label 1 is the Whole tumor
    label 2 is the Enhancing tumor
    The possible classes are TC (Tumor core), WT (Whole tumor)and ET (Enhancing tumor).

    Args:
        data: dictionary with the data to be transformed

    Returns:
        dictionary with the transformed data
    """
    def __call__(self, data):
        d = dict(data)
        for key in self.keys:
            result = []
            # merge label 2 and label 3 to construct TC
            result.append(torch.logical_or(d[key] == 2, d[key] == 3))
            # merge labels 1, 2 and 3 to construct WT
            result.append(torch.logical_or(torch.logical_or(d[key] == 2, d[key] == 3), d[key] == 1))
            # label 2 is ET
            result.append(d[key] == 2)
            d[key] = torch.stack(result, axis=0).float()
        return d